# Values below inherited from:
# https://gitlab.com/jdfant/terraform/-/blob/main/aws/vpc/main.tf?ref_type=heads#L23

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket         = "terraform-prod-remote-backend"
    key            = "prod-vpc/terraform.tfstate"
    dynamodb_table = "prod-vpc-state-lock"
    region         = "us-west-2"
  }
}