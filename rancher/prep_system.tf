resource "null_resource" "remote-exec" {
  connection {
    host        = var.nodes[0].internal_address
    user        = var.nodes[0].user
    private_key = file(var.private_ssh_key)
  }

  provisioner "file" {
    source      = "./scripts/iptables.sh"
    destination = "/tmp/iptables.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/iptables.sh",
      "/tmp/iptables.sh",
    ]
  }
}
