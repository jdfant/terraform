resource "rke_cluster" "cluster" {
  depends_on            = [null_resource.remote-exec]
  provider              = rke
  ssh_agent_auth        = false
  ignore_docker_version = false

  dynamic "nodes" {
    for_each = var.nodes
    content {
      hostname_override = nodes.value.hostname_override
      address           = nodes.value.address
      internal_address  = nodes.value.internal_address
      user              = nodes.value.user
      role              = nodes.value.role
      port              = nodes.value.port
      docker_socket     = nodes.value.docker_socket
      ssh_agent_auth    = nodes.value.ssh_agent_auth
      ssh_key           = file(var.private_ssh_key)
    }
  }

  services {
    etcd {
      backup_config {
        interval_hours = 6
        retention      = 24
      }
    }
  }

  authentication {
    strategy = "x509"
  }

  authorization {
    mode = "rbac"
  }

  ingress {
    provider = "nginx"
    options = {
      use-forwarded-headers = true
    }
  }
}
