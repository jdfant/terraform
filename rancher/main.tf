terraform {
  required_version = ">= 1.0"
  required_providers {
    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.1"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.1.1"
    }
    rke = {
      source  = "rancher/rke"
      version = "~> 1.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.6.0"
    }
  }
}

provider "local" {}

provider "null" {}

provider "rke" {
  log_file = var.rke_log
}

provider "kubernetes" {
  host     = rke_cluster.cluster.api_server_url
  username = rke_cluster.cluster.kube_admin_user

  client_certificate     = rke_cluster.cluster.client_cert
  client_key             = rke_cluster.cluster.client_key
  cluster_ca_certificate = rke_cluster.cluster.ca_crt
}

provider "helm" {
  kubernetes {
    config_path = "./kube_config_cluster.yml"
    #host     = rke_cluster.cluster.api_server_url
    #username = rke_cluster.cluster.kube_admin_user

    #client_certificate     = rke_cluster.cluster.client_cert
    #client_key             = rke_cluster.cluster.client_key
    #cluster_ca_certificate = rke_cluster.cluster.ca_crt
  }
}
