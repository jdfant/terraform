### Creates a 3 node RKE Kubernetes cluster (with 3 ETC services) on bare metal, then adds Rancher from Helm3 chart. ###

#### This is just a POC. NOT production ready (yet) ####

#### Kubernetes-Helm-Rancher graph: ####
![Kubernetes-Helm-Rancher_graph](images/kube-helm-rancher_graph.png)

**Still to do:**
- _Helm3 provider is still a bit flaky, may need to switch Rancher2 provider, for now :(_
- _Create additional Rancher/Kubernetes clusters._
- _Build, configure, populate external Docker registry._
- _Add CI/CD containers._
- _Add RancherOS (k3os??) installer?_
- _Create an Ansible wrapper for this to easily integrate https://gitlab.com/jdfant/ansible/tree/master/roles/lb for configuring external load balancers?_


<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_local"></a> [local](#provider\_local) | n/a |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |
| <a name="provider_rke"></a> [rke](#provider\_rke) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.rancher](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.cattle-system](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_secret.tls-ca](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.tls-rancher-ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [local_file.ca_crt](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [local_file.client_cert](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [local_file.client_key](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [local_file.kube_cluster_yaml](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [null_resource.remote-exec](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [rke_cluster.cluster](https://registry.terraform.io/providers/hashicorp/rke/latest/docs/resources/cluster) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ca_cert"></a> [ca\_cert](#input\_ca\_cert) | n/a | `string` | `"./certs/cacerts.pem"` | no |
| <a name="input_cluster_ca_cert"></a> [cluster\_ca\_cert](#input\_cluster\_ca\_cert) | n/a | `string` | `"./kube_certs/cacerts.pem"` | no |
| <a name="input_cluster_tls_cert"></a> [cluster\_tls\_cert](#input\_cluster\_tls\_cert) | n/a | `string` | `"./kube_certs/cert.pem"` | no |
| <a name="input_cluster_tls_key"></a> [cluster\_tls\_key](#input\_cluster\_tls\_key) | n/a | `string` | `"./kube_certs/key.pem"` | no |
| <a name="input_helm_hostname"></a> [helm\_hostname](#input\_helm\_hostname) | n/a | `string` | `"rancher.home.lan"` | no |
| <a name="input_helm_rancher_version"></a> [helm\_rancher\_version](#input\_helm\_rancher\_version) | n/a | `string` | `"rancher-latest"` | no |
| <a name="input_helm_repository"></a> [helm\_repository](#input\_helm\_repository) | n/a | `string` | `"https://releases.rancher.com/server-charts/latest"` | no |
| <a name="input_node_ip"></a> [node\_ip](#input\_node\_ip) | n/a | `list` | <pre>[<br>  "192.168.100.60",<br>  "192.168.100.61",<br>  "192.168.100.62"<br>]</pre> | no |
| <a name="input_private_ssh_key"></a> [private\_ssh\_key](#input\_private\_ssh\_key) | n/a | `string` | `"./keys/rancho"` | no |
| <a name="input_rke_log"></a> [rke\_log](#input\_rke\_log) | n/a | `string` | `"./rke.log"` | no |
| <a name="input_tls_cert"></a> [tls\_cert](#input\_tls\_cert) | n/a | `string` | `"./certs/cert.pem"` | no |
| <a name="input_tls_key"></a> [tls\_key](#input\_tls\_key) | n/a | `string` | `"./certs/key.pem"` | no |
| <a name="input_users"></a> [users](#input\_users) | n/a | `list` | <pre>[<br>  "root",<br>  "rancher",<br>  "jd"<br>]</pre> | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->