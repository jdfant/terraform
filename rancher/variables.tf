/*
 * RKE
 */

variable "rke_log" {
  type    = string
  default = "./rke.log"
}

variable "nodes" {
  type = list(object({
    hostname_override = string
    port              = string
    docker_socket     = string
    role              = list(string)
    ssh_agent_auth    = bool,
    address           = string
    internal_address  = string
    user              = string
  }))
  default = [
    {
      address           = "192.168.100.60"
      internal_address  = "192.168.100.60"
      hostname_override = "controller"
      port              = "22"
      docker_socket     = "/var/run/docker.sock"
      user              = "rke"
      role              = ["controlplane", "etcd"]
      ssh_agent_auth    = false
    },
    {
      address           = "192.168.100.61"
      internal_address  = "192.168.100.61"
      hostname_override = "node1"
      port              = "22"
      docker_socket     = "/var/run/docker.sock"
      user              = "rke"
      role              = ["worker"]
      ssh_agent_auth    = false
    },
    {
      address           = "192.168.100.62"
      internal_address  = "192.168.100.62"
      hostname_override = "node2"
      port              = "22"
      docker_socket     = "/var/run/docker.sock"
      user              = "rke"
      role              = ["worker"]
      ssh_agent_auth    = false
    },
  ]
}

/*
 * Helm
 */

variable "helm_hostname" {
  type    = string
  default = "rancher.home.lan"
}

variable "helm_repository" {
  type    = string
  default = "https://releases.rancher.com/server-charts/latest"
}

variable "helm_rancher_version" {
  type    = string
  default = "rancher-latest"
}

/*
 * Certs/Keys
 */

variable "private_ssh_key" {
  type    = string
  default = "./keys/rancho"
}

variable "cluster_tls_cert" {
  type    = string
  default = "./kube_certs/cert.pem"
}

variable "cluster_tls_key" {
  type    = string
  default = "./kube_certs/key.pem"
}

variable "cluster_ca_cert" {
  type    = string
  default = "./kube_certs/cacerts.pem"
}

variable "tls_cert" {
  type    = string
  default = "./certs/cert.pem"
}

variable "tls_key" {
  type    = string
  default = "./certs/key.pem"
}

variable "ca_cert" {
  type    = string
  default = "./certs/cacerts.pem"
}
